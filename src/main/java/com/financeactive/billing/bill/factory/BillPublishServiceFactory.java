package com.financeactive.billing.bill.factory;

import com.financeactive.billing.bill.service.BillPublishService;
import com.financeactive.billing.bill.service.BillPublishServiceText;
import com.financeactive.billing.dictionary.factory.DictionaryAccessorFactory;

public final class BillPublishServiceFactory {

    private static volatile BillPublishService instance;

    private BillPublishServiceFactory() {
    }

    public static BillPublishService instance() {
        if (instance == null) {
            synchronized (BillPublishService.class) {
                if (instance == null)
                    instance = createBillPublishTextService();
            }
        }
        return instance;
    }

    private static BillPublishService createBillPublishTextService() {
        BillPublishServiceText publishServiceText = new BillPublishServiceText();
        publishServiceText.setDictionaryAccessor(DictionaryAccessorFactory.instance());
        return publishServiceText;
    }

}
