package com.financeactive.billing.bill.factory;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.bill.entity.Bill;
import com.financeactive.billing.money.Amount;

public interface BillFactory {

    Bill createBill(Basket basket, Amount total, Amount totalTax);

}
