package com.financeactive.billing.bill.service;

import com.financeactive.billing.basket.entity.Basket;
import com.financeactive.billing.bill.entity.Bill;
import com.financeactive.billing.bill.factory.BillFactory;
import com.financeactive.billing.money.Amount;
import com.financeactive.billing.tax.calculator.TaxCalculator;
import lombok.Setter;

import static com.financeactive.billing.money.Amount.ZERO_AMOUNT;

public class BillingServiceImpl implements BillingService {

    private @Setter BillFactory billFactory;

    private @Setter TaxCalculator taxCalculator;

    @Override
    public Bill calculateBill(final Basket basket) {
        applyTax(basket);
        return billFactory.createBill(basket, totalAmount(basket), totalTax(basket));
    }

    private void applyTax(final Basket basket) {
        basket.getItems().forEach(b -> taxCalculator.applyTax(b.getProduct()));
    }

    private Amount totalTax(final Basket basket) {
        return basket.getItems().stream()
                .map(i -> i.getProduct().getTax().multiply(i.getQuantity()))
                .reduce(Amount::plus).orElse(ZERO_AMOUNT);
    }

    private Amount totalAmount(final Basket basket) {
        return basket.getItems().stream()
                .map(i -> i.getProduct().getTtc().multiply(i.getQuantity()))
                .reduce(Amount::plus).orElse(ZERO_AMOUNT);
    }

}
