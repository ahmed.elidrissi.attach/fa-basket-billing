package com.financeactive.billing.basket.factory;

import com.financeactive.billing.basket.entity.Basket;

public interface BasketFactory {

    Basket createEmptyBasket();

}
