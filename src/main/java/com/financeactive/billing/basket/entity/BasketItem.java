package com.financeactive.billing.basket.entity;

import com.financeactive.billing.money.Amount;
import com.financeactive.billing.product.entity.Product;
import lombok.Data;

@Data
public class BasketItem {

    private final Product product;

    private final int quantity;

    BasketItem(final Product product, final int quantity) {
        this.product = product;
        this.quantity = quantity;
    }


    BasketItem increment() {
        return new BasketItem(product, quantity + 1);
    }

    public Amount getTotalTTC() {
        return product.getTtc().multiply(quantity);
    }

}
