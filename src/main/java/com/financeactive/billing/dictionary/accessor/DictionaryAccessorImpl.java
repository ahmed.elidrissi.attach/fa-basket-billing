package com.financeactive.billing.dictionary.accessor;

import com.financeactive.billing.dictionary.entity.Dictionary;
import com.financeactive.billing.dictionary.factory.DictionaryFactory;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import lombok.Setter;
import net.minidev.json.JSONArray;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class DictionaryAccessorImpl implements DictionaryAccessor {

    private @Setter DictionaryFactory dictionaryFactory;

    @Override
    public Dictionary findByDomainAndCode(final String domain, final String code) {
        return filter(loadDictionary(), domain, code);
    }

    private Dictionary filter(final Set<Dictionary> dictionaries, final String domain, final String code) {
        return dictionaries.stream()
                .filter(d -> d.getDomain().equals(domain))
                .filter(d -> d.getCode().equals(code))
                .findFirst().orElse(null);
    }

    private Set<Dictionary> loadDictionary() {
        Object document = Configuration.defaultConfiguration().jsonProvider().parse(readDataFile());
        JSONArray values = JsonPath.read(document, "$.[*]");
        return values.stream()
                .map(m -> (Map) m)
                .map(m -> dictionaryFactory.createDictionary(lookupDomain(m), lookupCode(m), lookupLabel(m)))
                .collect(toSet());
    }

    private String readDataFile() {
        try {
            final InputStream stream = this.getClass().getResourceAsStream("/data/dictionary.json");
            return IOUtils.toString(stream, Charset.forName("UTF-8"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String lookupDomain(final Map m) {
        return m.get("domain").toString();
    }

    private String lookupCode(final Map m) {
        return m.get("code").toString();
    }

    private String lookupLabel(final Map m) {
        return m.get("label").toString();
    }

}
