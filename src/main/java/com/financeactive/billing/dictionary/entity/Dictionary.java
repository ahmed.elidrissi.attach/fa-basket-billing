package com.financeactive.billing.dictionary.entity;

import lombok.Data;

@Data
public class Dictionary {

    private final String domain;

    private final String code;

    private final String label;

}
