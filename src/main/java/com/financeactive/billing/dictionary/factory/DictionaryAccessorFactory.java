package com.financeactive.billing.dictionary.factory;

import com.financeactive.billing.dictionary.accessor.DictionaryAccessor;
import com.financeactive.billing.dictionary.accessor.DictionaryAccessorImpl;

public final class DictionaryAccessorFactory {

    private static volatile DictionaryAccessor instance;

    private DictionaryAccessorFactory() {
    }

    public static DictionaryAccessor instance() {
        if (instance == null) {
            synchronized (DictionaryAccessor.class) {
                if (instance == null)
                    instance = createDictionaryAccessor();
            }
        }
        return instance;
    }

    private static DictionaryAccessor createDictionaryAccessor() {
        DictionaryAccessorImpl accessor = new DictionaryAccessorImpl();
        accessor.setDictionaryFactory(DictionaryFactoryImpl.instance());
        return accessor;
    }


}
