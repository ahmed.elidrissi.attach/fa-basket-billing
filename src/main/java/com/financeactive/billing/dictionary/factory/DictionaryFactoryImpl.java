package com.financeactive.billing.dictionary.factory;

import com.financeactive.billing.dictionary.entity.Dictionary;

public class DictionaryFactoryImpl implements DictionaryFactory {

    private static volatile DictionaryFactoryImpl instance;

    public static DictionaryFactoryImpl instance() {
        if (instance == null) {
            synchronized (DictionaryFactoryImpl.class) {
                if (instance == null)
                    instance = new DictionaryFactoryImpl();
            }
        }
        return instance;
    }

    @Override
    public Dictionary createDictionary(final String domain, final String code, final String label) {
        return new Dictionary(domain, code, label);
    }

}
