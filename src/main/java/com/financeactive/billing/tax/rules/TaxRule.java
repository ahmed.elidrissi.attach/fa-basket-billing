package com.financeactive.billing.tax.rules;

import com.financeactive.billing.product.entity.Product;

public abstract class TaxRule {

    private TaxRule next;

    public void apply(final Product product) {
        process(product);
        if (next != null) next.apply(product);
    }

    abstract void process(Product product);

    public TaxRule setNextRule(final TaxRule rule) {
        this.next = rule;
        return this;
    }

}
