package com.financeactive.billing.tax.rules;

import com.financeactive.billing.money.Amount;
import com.financeactive.billing.product.entity.Product;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.financeactive.billing.money.Amount.newAmount;
import static java.lang.Integer.parseInt;

public class LastRoundTaxRule extends TaxRule {

    private static final int FIVE = 5;
    private static final int TEN = 10;

    @Override
    void process(final Product product) {
        product.setTax(product.getTax().plus(lastTaxRound(product)));
    }

    private Amount lastTaxRound(final Product product) {
        return newAmount("0.0".concat(Integer.toString(lastTaxRoundDigit(product))));
    }

    private int lastTaxRoundDigit(final Product product) {
        int digit = 0;
        if (doesRangeContainDigit(downRange(), lastTaxDigit(product))) {
             digit = FIVE - lastTaxDigit(product);
        } else if (doesRangeContainDigit(upRange(), lastTaxDigit(product))) {
            digit = TEN - lastTaxDigit(product);
        }
        return digit;
    }

    private IntStream downRange() {
        //CHECKSTYLE:OFF
        return IntStream.rangeClosed(1, 4);
        //CHECKSTYLE:ON
    }

    private IntStream upRange() {
        //CHECKSTYLE:OFF
        return IntStream.rangeClosed(6, 9);
        //CHECKSTYLE:ON
    }

    private int lastTaxDigit(final Product product) {
        return parseInt(product.getTax().asString().substring(product.getTax().asString().length() - 1));
    }

    private boolean doesRangeContainDigit(final IntStream range, final int digit) {
        return range.boxed().collect(Collectors.toList()).contains(digit);
    }

}
