package com.financeactive.billing.product.entity;

import lombok.Data;

@Data
public class ProductType {

    private final ProductCategory category;

    private final String code;

    private final boolean imported;

    public ProductType(final ProductCategory category, final String code, final boolean imported) {
        this.category = category;
        this.code = code;
        this.imported = imported;
    }

}
