package com.financeactive.billing.product.entity;

import lombok.Data;

@Data
public class ProductCategory {

    private final String code;

}
