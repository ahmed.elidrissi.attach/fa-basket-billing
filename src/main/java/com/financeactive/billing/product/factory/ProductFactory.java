package com.financeactive.billing.product.factory;

import com.financeactive.billing.money.Amount;
import com.financeactive.billing.product.entity.Product;
import com.financeactive.billing.product.entity.ProductCategory;
import com.financeactive.billing.product.entity.ProductType;

public interface ProductFactory {

    Product createProduct(String id, ProductType type, Amount price);

    ProductType createType(ProductCategory category, String code, boolean imported);

    ProductType createType(ProductCategory category, String code);

    ProductCategory createCategory(String code);
}
