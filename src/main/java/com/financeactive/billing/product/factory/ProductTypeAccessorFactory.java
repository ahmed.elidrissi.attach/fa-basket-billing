package com.financeactive.billing.product.factory;

import com.financeactive.billing.product.accessor.ProductTypeAccessor;
import com.financeactive.billing.product.accessor.ProductTypeAccessorImpl;

public final class ProductTypeAccessorFactory {

    private static volatile ProductTypeAccessor instance;

    private ProductTypeAccessorFactory() {
    }

    public static ProductTypeAccessor instance() {
        if (instance == null) {
            synchronized (ProductTypeAccessor.class) {
                if (instance == null)
                    instance = createProductTypeAccessor();
            }
        }
        return instance;
    }


    private static ProductTypeAccessor createProductTypeAccessor() {
        ProductTypeAccessorImpl productTypeAccessor = new ProductTypeAccessorImpl();
        productTypeAccessor.setCategoryAccessor(ProductCategoryAccessorFactory.instance());
        return productTypeAccessor;
    }

}
