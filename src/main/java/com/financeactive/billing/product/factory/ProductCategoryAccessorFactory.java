package com.financeactive.billing.product.factory;

import com.financeactive.billing.product.accessor.ProductCategoryAccessor;
import com.financeactive.billing.product.accessor.ProductCategoryAccessorImpl;

public final class ProductCategoryAccessorFactory {

    private static volatile ProductCategoryAccessor instance;

    private ProductCategoryAccessorFactory() {
    }

    public static ProductCategoryAccessor instance() {
        if (instance == null) {
            synchronized (ProductCategoryAccessor.class) {
                if (instance == null)
                    instance = createProductCategoryAccessor();
            }
        }
        return instance;
    }

    private static ProductCategoryAccessor createProductCategoryAccessor() {
        return new ProductCategoryAccessorImpl();
    }

}
