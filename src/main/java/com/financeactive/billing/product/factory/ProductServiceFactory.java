package com.financeactive.billing.product.factory;

import com.financeactive.billing.product.service.ProductService;
import com.financeactive.billing.product.service.ProductServiceImpl;

public final class ProductServiceFactory {

    private static volatile ProductService instance;

    private ProductServiceFactory() {
    }

    public static ProductService instance() {
        if (instance == null) {
            synchronized (ProductService.class) {
                if (instance == null)
                    instance = createProductService();
            }
        }
        return instance;
    }

    private static ProductService createProductService() {
        final ProductServiceImpl productService = new ProductServiceImpl();
        productService.setProductTypeAccessor(ProductTypeAccessorFactory.instance());
        return productService;
    }

}
