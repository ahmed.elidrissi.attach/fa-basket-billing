package com.financeactive.billing.product.factory;

import com.financeactive.billing.money.Amount;
import com.financeactive.billing.product.entity.Product;
import com.financeactive.billing.product.entity.ProductCategory;
import com.financeactive.billing.product.entity.ProductType;

public class ProductFactoryImpl implements ProductFactory {

    private static volatile ProductFactoryImpl instance;

    public static ProductFactoryImpl instance() {
        if (instance == null) {
            synchronized (ProductFactoryImpl.class) {
                if (instance == null)
                    instance = new ProductFactoryImpl();
            }
        }
        return instance;
    }

    @Override
    public Product createProduct(final String id, final ProductType type, final Amount price) {
        return new Product(id, type, price);
    }

    @Override
    public ProductType createType(final ProductCategory cat, final String cod, final boolean imp) {
        return new ProductType(cat, cod, imp);
    }

    @Override
    public ProductType createType(final ProductCategory cat, final String cod) {
        return createType(cat, cod, false);
    }

    @Override
    public ProductCategory createCategory(final String code) {
        return new ProductCategory(code);
    }
}
