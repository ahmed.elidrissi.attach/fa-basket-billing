package com.financeactive.billing.product.exception;

public class UndefinedProductTypeException extends RuntimeException {

    public UndefinedProductTypeException(final String value) {
        super(String.format("The product type <%s> is not defined", value));
    }

}
