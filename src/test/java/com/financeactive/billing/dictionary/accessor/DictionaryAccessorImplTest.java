package com.financeactive.billing.dictionary.accessor;

import com.financeactive.billing.dictionary.entity.Dictionary;
import com.financeactive.billing.dictionary.factory.DictionaryFactoryImpl;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

class DictionaryAccessorImplTest {

    private static Stream<Arguments> badValueExamples() {
        return Stream.of(
                Arguments.of("PRODUCT-TYPE", "FR-BOOK", "livre"),
                Arguments.of("PRODUCT-TYPE", "CHOCOLATE-BOX-IMP", "boîte de chocolats importée"),
                Arguments.of("PRODUCT-TYPE", "HEADACHE-PILLS-BOX", "boîte de pilules contre la migraine")
        );
    }

    @ParameterizedTest
    @MethodSource("badValueExamples")
    void when_find_by_domain_and_code_then_return_value(final String domain, final String code, final String label) {
        DictionaryAccessorImpl accessor = new DictionaryAccessorImpl();
        accessor.setDictionaryFactory(DictionaryFactoryImpl.instance());
        final Dictionary dictionary = accessor.findByDomainAndCode(domain, code);
        assertThat(dictionary, notNullValue());
        assertThat(dictionary.getLabel(), Matchers.equalTo(label));
    }

}
